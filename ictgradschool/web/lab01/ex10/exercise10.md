In this exercise, you will simply convert the following collection of elements into an unordered list.

<ul>

<li>Auckland (North Island)

<li>Hamilton (North Island)

<li>Wellington (North Island)

<li>Christchurch (South Island)

<li>Dunedin (South Island)

<li>Gore (South Island)

<li>Gisborne (North Island)

<li>Napier (North Island)

<li>Taupo (North Island)

<li>Rotorua (North Island)

</ul>
