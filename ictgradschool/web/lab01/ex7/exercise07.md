<strong>Herbivorous</strong>
<br>*Horse
<br>*Cow
<br>*Sheep
<br>*Chicken

<strong>Carnivorous</strong>
<br>*Cat
<br>*Dog
<br>*Lion

<strong>Omnivorous</strong>
<br>*Polar Bear
<br>*Frog
<br>*Rat
<br>*Hawk
<br>*Fox
<br>*Capybara
<br>*Raccoon
<br>*Fennec Fox
